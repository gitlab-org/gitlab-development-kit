# frozen_string_literal: true

module GDK
  module PackageConfig
    PROJECTS = {
      gitaly: {
        package_name: 'gitaly',
        package_version: 'main',
        project_path: 'gitaly',
        upload_path: 'build',
        download_path: '_build/bin',
        platform_specific: true
      },
      gitlab_shell: {
        package_name: 'gitlab-shell',
        package_version: 'main',
        project_path: 'gitlab-shell',
        upload_path: 'build',
        download_path: 'bin',
        platform_specific: true
      },
      workhorse: {
        package_name: 'workhorse',
        package_version: 'main',
        project_path: 'gitlab/workhorse',
        upload_path: 'build',
        download_path: '.',
        platform_specific: true
      },
      graphql_schema: {
        package_name: 'graphql-schema',
        package_version: 'master',
        project_path: 'gitlab',
        upload_path: 'tmp/tests/graphql', # uploaded in gitlab-org/gitlab
        download_path: 'tmp/tests/graphql',
        platform_specific: false
      }
    }.freeze

    def self.project(name)
      data = PROJECTS[name]
      project_path = GDK.config.gdk_root.join(data[:project_path])

      data.merge(
        package_path: "#{data[:package_name]}.tar.gz",
        project_path: project_path,
        upload_path: project_path.join(data[:upload_path]),
        download_path: project_path.join(data[:download_path])
      )
    end
  end
end
