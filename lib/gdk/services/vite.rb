# frozen_string_literal: true

module GDK
  module Services
    # Rails web frontend server
    class Vite < Base
      def name
        'vite'
      end

      def command
        %(support/exec-cd gitlab bundle exec vite dev)
      end

      def enabled?
        config.vite.enabled?
      end

      def validate!
        return unless config.vite? && config.webpack?

        raise GDK::ConfigSettings::UnsupportedConfiguration, <<~MSG.strip
          Running vite and webpack at the same time is unsupported.
          Consider running `gdk config set webpack.enabled false` to disable webpack
        MSG
      end

      def env
        e = {
          GITLAB_UI_WATCH: config.gitlab_ui?,
          VITE_RUBY_PORT: config.vite.port
        }

        e[:VUE_VERSION] = config.vite.vue_version if config.vite.vue_version == 3

        e
      end
    end
  end
end
