# frozen_string_literal: true

module GDK
  module Services
    class Consul < Base
      def name
        'consul'
      end

      def command
        "consul agent -config-file #{config.gdk_root.join('consul/config.json')} -dev"
      end

      def env
        { 'PGPASSWORD' => 'gitlab' }
      end

      def enabled?
        config.load_balancing.discover?
      end
    end
  end
end
