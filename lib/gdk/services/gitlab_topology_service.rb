# frozen_string_literal: true

require 'pathname'

module GDK
  module Services
    class GitlabTopologyService < Base
      def name
        'gitlab-topology-service'
      end

      def command
        "support/exec-cd gitlab-topology-service go run . serve"
      end

      def ready_message
        'The TopologyService is up and running.'
      end

      def enabled?
        config.gitlab_topology_service.enabled?
      end
    end
  end
end
