# frozen_string_literal: true

module GDK
  module Diagnostic
    # Notifies users about the supported tool version manager and suggests migrating from asdf to mise.
    class ToolVersionManager < Base
      TITLE = 'Tool Version Manager'
      BROKEN_ASDF_VERSION = /v0\.16\.\d+/

      def success?
        using_mise? || using_custom_manager?
      end

      def detail
        return if success?

        messages = []
        messages << <<~MESSAGE if broken_asdf_version?
          ERROR: Your installed version of asdf (`#{current_asdf_version}`) has a bug that makes it incompatible with GDK.
          Please downgrade to `v0.15.0` or switch to `mise`.
        MESSAGE

        messages << <<~MESSAGE
          Starting April 1st, 2025, mise will replace asdf as the supported tool version manager for GDK.

          The main benefit of mise is better performance, and it removes the manual setup we had to do
          with asdf, like installing dependencies in the right order when they depend on each other.
          There's no need to install plugins separately as mise takes care of everything.

          You are currently using asdf. To migrate to mise, run:

          1. Go to your GDK directory:
            cd "#{GDK.config.gdk_root}"

          2. Run the migration task:
            bundle exec rake mise:migrate

          If you run into any issues during migration, please let us know in this issue: https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/2360.
        MESSAGE

        messages.join("\n")
      end

      private

      def using_mise?
        GDK.config.asdf.opt_out? && GDK.config.mise.enabled?
      end

      def using_custom_manager?
        GDK.config.asdf.opt_out? && !GDK.config.mise.enabled?
      end

      def broken_asdf_version?
        BROKEN_ASDF_VERSION.match?(current_asdf_version)
      end

      def current_asdf_version
        @current_asdf_version ||= GDK::Shellout.new('asdf').readlines.first
      rescue Errno::ENOENT
        @current_asdf_version = ''
      end
    end
  end
end
