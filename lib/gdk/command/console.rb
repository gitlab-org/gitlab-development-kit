# frozen_string_literal: true

module GDK
  module Command
    # Run IRB console with GDK environment loaded
    class Console < BaseCommand
      def run(_ = [])
        console_args = %w[irb -I lib -r gdk]
        exec(*console_args, chdir: GDK.root)
      end
    end
  end
end
