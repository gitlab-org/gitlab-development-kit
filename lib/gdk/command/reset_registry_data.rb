# frozen_string_literal: true

module GDK
  module Command
    # Handles `gdk reset-registry-data` command execution
    class ResetRegistryData < BaseCommand
      def run(_ = [])
        return false unless continue?

        execute
      end

      private

      def continue?
        GDK::Output.warn("We're about to remove Container Registry PostgreSQL data.")

        return true unless GDK::Output.interactive?

        GDK::Output.prompt('Are you sure? [y/N]').match?(/\Ay(?:es)*\z/i)
      end

      def execute
        manager = GDK::RegistryDatabaseManager.new(GDK.config)
        manager.reset_registry_database
      end
    end
  end
end
