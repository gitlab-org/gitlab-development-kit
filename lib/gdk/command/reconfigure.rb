# frozen_string_literal: true

module GDK
  module Command
    # Handles `gdk reconfigure` command execution
    class Reconfigure < BaseCommand
      def run(_args = [])
        unless run_rake('gdk-config.mk')
          out.error("Failed to generate gdk.config.yml, check your gdk.yml.")
          false
        end

        # already done in `rake gdk-config.mk`
        ENV['SKIP_GENERATE_GDK_CONFIG_MK'] = '1'

        diff = diff_config
        success = run_rake(:reconfigure)

        if success
          out.success('Successfully reconfigured!')

          unless diff.empty?
            out.puts
            out.puts diff unless diff.empty?
          end
        else
          out.error('Failed to reconfigure.')
          display_help_message
        end

        success
      rescue Support::Rake::TaskWithLogger::LoggerError => e
        e.print!
        false
      end

      private

      def diff_config
        Diagnostic::Configuration.new.config_diff
      end
    end
  end
end
