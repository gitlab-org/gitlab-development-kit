# frozen_string_literal: true

# GitLab Development Kit CLI parser / executor
#
# This file is loaded by the 'gdk' command in the gem. This file is NOT
# part of the gitlab-development-kit gem so that we can iterate faster.

$LOAD_PATH.unshift(__dir__)

require 'pathname'
require 'securerandom'
require 'zeitwerk'

loader = Zeitwerk::Loader.new
loader.tag = File.basename(__FILE__, '.rb')
loader.inflector.inflect(
  {
    'gdk' => 'GDK',
    'http_helper' => 'HTTPHelper',
    'open_ldap' => 'OpenLDAP',
    'test_url' => 'TestURL'
  })
loader.push_dir(__dir__)
loader.setup

# GitLab Development Kit
module GDK
  StandardErrorWithMessage = Class.new(StandardError)
  HookCommandError = Class.new(StandardError)

  # requires `gitlab-development-kit` gem to be at least this version
  REQUIRED_GEM_VERSION = '0.2.18'
  PROGNAME = 'gdk'
  MAKE = RUBY_PLATFORM.include?('bsd') ? 'gmake' : 'make'

  # Entry point for the GDK binary.
  #
  # Do not remove because we need to support that use case where a new GDK binary
  # calls older GDK code.
  def self.main
    setup_rake

    Command.run(ARGV)
  end

  def self.setup_rake
    require 'rake'
    Rake.application.init('rake', %W[--rakefile #{GDK.root}/Rakefile])
    Rake.application.load_rakefile
  end

  def self.config
    @config ||= GDK::Config.load_from_file
  end

  # Return the path to the GDK base path
  #
  # @return [Pathname] path to GDK base directory
  def self.root
    Pathname.new(__dir__).parent
  end

  def self.make(*targets, env: {})
    sh = Shellout.new(MAKE, targets, chdir: GDK.root, env: env)
    sh.stream
    sh
  end
end
