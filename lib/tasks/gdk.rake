# frozen_string_literal: true

namespace :gdk do
  migrations = %w[
    migrate:fix_telemetry_user
  ]

  desc "Run migration related to GDK setup"
  task migrate: migrations

  namespace :migrate do
    desc "Replaces hardcoded `telemetry_user` with an unique one"
    task :fix_telemetry_user do
      if GDK.config.telemetry? && GDK.config.telemetry.username == 'telemetry_user'
        GDK::Output.info 'Fixing hardcoded telemetry username.'
        GDK::Telemetry.update_settings('')
      end
    end
  end
end
