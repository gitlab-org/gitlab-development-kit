# frozen_string_literal: true

namespace :mise do
  desc 'Migrate from asdf to mise'
  task :migrate do
    AsdfToMise.new.execute
  end
end

class AsdfToMise
  def execute
    disable_asdf
    enable_mise
    install_git_hooks
    remove_bootstrap_cache
    re_bootstrap
    replace_asdf_with_mise_in_shell_configs
    display_success_message
  end

  private

  def disable_asdf
    GDK::Output.info('Opting out of asdf...')
    GDK::Command::Config.new.run(['set', 'asdf.opt_out', 'true'])
  end

  def replace_asdf_with_mise_in_shell_configs
    @updated_shell_configs = []
    @backup_paths = []

    mise_path = `which mise 2>/dev/null`.strip
    mise_path = 'mise' if mise_path.empty?

    shell_configs = {
      '~/.bashrc' => { asdf: 'asdf.sh', mise: "eval \"$(#{mise_path} activate bash)\"" },
      '~/.zshrc' => { asdf: 'asdf.sh', mise: "eval \"$(#{mise_path} activate zsh)\"" },
      '~/.config/fish/config.fish' => { asdf: 'asdf.fish', mise: 'mise activate fish' },
      '~/.config/elvish/rc.elv' => { asdf: 'asdf.elv', mise: 'mise activate elvish' },
      '~/.config/nushell/config.nu' => { asdf: 'asdf.nu', mise: 'mise activate nu' }
    }

    shell_configs.each do |path, scripts|
      full_path = File.expand_path(path)
      next unless File.file?(full_path)

      content = File.read(full_path)
      original = content.dup

      mise_line = "# Added by GDK bootstrap\n#{scripts[:mise]}"
      pattern = %r{# Added by GDK bootstrap\n(\.|source) .*?/\.asdf/#{Regexp.escape(scripts[:asdf])}}

      next unless content.match?(pattern)

      content = content.gsub(pattern, mise_line)
      next if content == original

      backup_path = "#{full_path}.#{Time.now.strftime('%Y%m%d%H%M%S')}.bak"
      FileUtils.cp(full_path, backup_path)
      @backup_paths << backup_path

      File.write(full_path, content)
      @updated_shell_configs << full_path
    rescue StandardError => e
      GDK::Output.error("Failed to update #{full_path}: #{e.message}")
    end
  end

  def enable_mise
    GDK::Output.info('Enabling mise...')
    GDK::Command::Config.new.run(['set', 'mise.enabled', 'true'])
  end

  def install_git_hooks
    GDK::Output.info('Installing Git hooks...')
    run_command('lefthook install', 'lefthook install failed!')
  end

  def remove_bootstrap_cache
    GDK::Output.info('Removing cached bootstrap files...')
    cache_path = File.join(GDK.config.gdk_root, '.cache')
    %w[.gdk_bootstrapped .gdk_platform_setup].each do |file|
      FileUtils.rm_f(File.join(cache_path, file))
    end
  end

  def re_bootstrap
    GDK::Output.info('Running `bin/gdk-shell support/bootstrap` to install mise and dependencies...')
    run_command('bin/gdk-shell support/bootstrap', 'bin/gdk-shell support/bootstrap failed!')
  end

  def display_success_message
    GDK::Output.success('Migration from asdf to mise is almost complete!')
    GDK::Output.puts

    if @updated_shell_configs && !@updated_shell_configs.empty?
      GDK::Output.notice('Shell config files updated:')
      GDK::Output.puts("   - #{@updated_shell_configs.join("\n   - ")}")
      GDK::Output.puts
      GDK::Output.notice('Backups of your original shell config files were created:')
      GDK::Output.puts("   - #{@backup_paths.join("\n   - ")}")
      GDK::Output.puts
    end

    GDK::Output.notice('Next steps:')
    GDK::Output.notice('1. Please restart your terminal.')
    GDK::Output.notice('2. Afterward, run this command:')
    GDK::Output.puts('   gdk reconfigure && gdk update')
    GDK::Output.puts
    GDK::Output.notice('If you encounter any issues with mise, see our troubleshooting guide: https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/troubleshooting/mise.md')
  end

  def run_command(command, error_message)
    Bundler.with_unbundled_env do
      sh = GDK::Shellout.new(command.split, chdir: GDK.config.gdk_root).execute
      raise StandardError, error_message unless sh.success?
    end
  end
end
