# frozen_string_literal: true

require 'fileutils'

module Support
  module Rake
    class Update
      CORE_TARGETS = %w[
        gitlab-git-pull
        gitlab-setup
        postgresql
        gitlab/doc/api/graphql/reference/gitlab_schema.json
        preflight-checks
        preflight-update-checks
        gitaly-update
        ensure-databases-setup
        gitlab-shell-update
        unlock-dependency-installers
        gitlab-translations-unlock
        gitlab-workhorse-update
      ].freeze

      def self.make_tasks(config: GDK.config)
        core_tasks + optional_tasks(config)
      end

      def self.core_tasks
        CORE_TARGETS.map { |target| make_task(target) }
      end

      # rubocop:disable Metrics/AbcSize
      def self.optional_tasks(config)
        [
          make_task('gitlab-http-router-update', enabled: config.gitlab_http_router.enabled?),
          make_task('gitlab-topology-service-update', enabled: config.gitlab_topology_service.enabled?),
          make_task('docs-gitlab-com-update', enabled: config.docs_gitlab_com.enabled?),
          make_task('gitlab-elasticsearch-indexer-update', enabled: config.elasticsearch.enabled?),
          make_task('gitlab-k8s-agent-update', enabled: config.gitlab_k8s_agent.enabled?),
          make_task('gitlab-pages-update', enabled: config.gitlab_pages.enabled?),
          make_task('gitlab-ui-update', enabled: config.gitlab_ui.enabled?),
          make_task('gitlab-zoekt-indexer-update', enabled: config.zoekt.enabled?),
          make_task('gitlab-ai-gateway-update', enabled: config.gitlab_ai_gateway.enabled?),
          make_task('grafana-update', enabled: config.grafana.enabled?),
          make_task('jaeger-update', enabled: config.tracer.jaeger.enabled?),
          make_task('object-storage-update', enabled: config.object_store.enabled?),
          make_task('pgvector-update', enabled: config.pgvector.enabled?),
          make_task('zoekt-update', enabled: config.zoekt.enabled?),
          make_task('duo-workflow-service-update', enabled: config.duo_workflow.enabled?),
          make_task('openbao-update', enabled: config.openbao.enabled?),
          make_task('gitlab-runner-update', enabled: config.gitlab_runner.enabled?),
          make_task('siphon-update', enabled: config.siphon.enabled?),
          make_task('nats-update', enabled: config.nats.enabled?)
        ]
      end
      # rubocop:enable Metrics/AbcSize

      def self.make_task(target, enabled: true)
        MakeTask.new(target: target, enabled: enabled)
      end
    end
  end
end
