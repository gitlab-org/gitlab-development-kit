# frozen_string_literal: true

module Support
  module Rake
    module TaskWithLogger
      MakeError = Class.new(StandardError) do
        def initialize(target)
          super("`make #{target}` failed.")
        end
      end

      LoggerError = Class.new(StandardError) do
        def initialize(task, error, logger)
          @task = task
          @error = error
          @logger = logger
          super("Task `#{@task.name}` failed")
        end

        def print!
          tail = @logger.tail.strip.split("\n").map { |l| "  #{l}" }.join("\n")
          GDK::Output.error("Task \e[32m#{@task.name}\e[0m failed:\n\n#{tail}")
          GDK::Output.puts
        end
      end

      def execute(...)
        # gdk-config.mk is included in the Makefile, so it must output
        # nothing, ever.
        return super if name == "gdk-config.mk"
        return super unless TaskWithSpinner.spinner_manager

        begin
          logger = TaskLogger.new(self)
          TaskLogger.set_current!(logger)
          super
          logger.cleanup!
        rescue StandardError => e
          if logger
            unless e.is_a?(MakeError)
              warn e.message
              warn e.backtrace
            end

            raise LoggerError.new(self, e, logger)
          end

          raise
        ensure
          logger&.cleanup!(delete: false)
          TaskLogger.set_current!(nil)
        end
      end
    end
  end
end
