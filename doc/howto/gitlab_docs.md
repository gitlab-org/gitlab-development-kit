# GitLab Docs in GDK

You can use the GDK to contribute GitLab documentation. The GDK can:

- Maintain a clone of the [`docs-gitlab-com`](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com) repository
  for work on changes to that project.
- Preview changes made in the GDK-managed `gitlab/doc` directory.

If you want to contribute to GitLab documentation without using GDK, see
[Set up local development and preview](https://gitlab.com/gitlab-org/technical-writing/docs-gitlab-com/-/blob/main/doc/setup.md).

## Configure GitLab Docs in GDK

GDK provides several configuration options.

### Enable GitLab Docs previews

To enable previewing GitLab documentation by using the `docs-gitlab-com` project:

1. Enable the `docs-gitlab-com` integration:

   ```shell
   gdk config set docs_gitlab_com.enabled true
   ```

1. Reconfigure GDK:

   ```shell
   gdk reconfigure
   ```

### Disable GitLab Docs previews

To disable previewing GitLab documentation by using the `docs-gitlab-com` project:

1. Disable the `docs-gitlab-com` integration:

   ```shell
   gdk config set docs_gitlab_com.enabled false
   ```

1. Reconfigure GDK:

   ```shell
   gdk reconfigure
   ```

### Disable automatic updates

To avoid automatically updating the `docs-gitlab-com` checkout, run:

```shell
gdk config set docs_gitlab_com.auto_update false
```

### Configure a custom port

The default port is `1313` but this can be customized:

```shell
gdk config set docs_gitlab_com.port 1314
```

### Configure HTTPS

You can run the GitLab Docs site using HTTPS. For more information, see [NGINX](nginx.md).

### Include more documentation

The full published documentation suite [includes additional documentation](https://docs.gitlab.com/development/documentation/site_architecture/)
from outside the [`gitlab` project](https://gitlab.com/gitlab-org/gitlab).

To make and preview changes to the additional documentation:

1. Run the following commands as required:

   ```shell
   gdk config set gitlab_runner.enabled true
   gdk config set omnibus_gitlab.enabled true
   gdk config set charts_gitlab.enabled true
   gdk config set gitlab_operator.enabled true
   ```

1. Run `gdk update` to:
   - Clone the additional projects for the first time, or update existing local copies.
   - Compile a published version of the additional documentation.
1. Start the `docs-gitlab-com` service if not already running:

   ```shell
   gdk start docs-gitlab-com
   ```

NOTE:
`gitlab_runner` should not be confused with [`runner`](runner.md).

By default, the cloned repositories of the `gitlab_runner`, `omnibus_gitlab`, `charts_gitlab`, and `gitlab_operator`
components are:

- Updated automatically when you run `gdk update`. To disable this, set `auto_update: false` against
  whichever project to disable.
- Cloned using HTTPS. If you originally [cloned `gitlab` using SSH](../index.md#use-gdk-to-install-gitlab), you
  might want to set these cloned repositories to SSH also. To set these repositories to SSH:

  1. Go into each cloned repository and run `git remote -v` to review the current settings.
  1. To switch to SSH, run `git remote set-url <remote name> git@gitlab.com:gitlab-org/<project path>.git`.
     For example, to update your HTTPS-cloned `gitlab-runner` repository (with a `remote` called
     `origin`), run:

     ```shell
     cd <GDK root path>/gitlab-runner
     git remote set-url origin git@gitlab.com:gitlab-org/gitlab-runner.git
     ```

  1. Run `git remote -v` in each cloned repository to verify that you have successfully made the change from
     HTTPS to SSH.

## Make and preview documentation changes

You can preview documentation changes as they would appear when published on
[GitLab Docs](https://docs.gitlab.com).

To make changes to GitLab documentation and preview them:

1. Start the `docs-gitlab-com` service and ensure you can preview the documentation site:

   ```shell
   gdk start docs-gitlab-com
   ```

1. Make the necessary changes to the files in `<path_to_gdk>/gitlab/doc`.
1. View the preview. You must restart the `docs-gitlab-com` service to recompile the published version of the documentation
   with the new changes:

   ```shell
   gdk restart docs-gitlab-com
   ```

   You can `tail` the `docs-gitlab-com` logs to see progress on rebuilding the documentation:

   ```shell
   gdk tail docs-gitlab-com
   ```

## Troubleshooting

### Documentation from disabled projects appears in preview

Disabling [additional documentation projects](#include-more-documentation) doesn't remove them
from your file system and Hugo continues to use them as a source of documentation. When disabled,
the projects aren't updated so Hugo is using old commits to preview the data from those projects.

To ensure only enabled projects appear in the preview:

1. Disable any projects you don't want previewed.
1. Remove the cloned project directory from inside your GDK directory.
