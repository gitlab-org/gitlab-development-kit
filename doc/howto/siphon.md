# Configure Siphon to run in GDK

You can configure Siphon to run in GDK. **Siphon is not yet ready for production use.**

Siphon facilitates data synchronization between PostgreSQL and other data stores.
You can use Siphon in GDK to synchronize data between tables in PostgreSQL to matching tables in ClickHouse,
which allows you to write features that will benefit from the performance improvements of using an OLAP database.

## Prerequisites

Before configuring Siphon to run in GDK, you must:

- Enable ClickHouse in your GDK. For more information, see
  [ClickHouse within GitLab](https://docs.gitlab.com/ee/development/database/clickhouse/clickhouse_within_gitlab.html).
- Enable NATS in your GDK. `gdk config set nats.enabled true`
- Configure a Docker runtime. Only Docker Desktop is supported, but other container runtimes might work.

## Enable logical replication in PostgreSQL

To use Siphon, you must enable logical replication for GDK's main PostgreSQL database:

1. Open the `$GDK_ROOT/postgresql/data/postgresql.conf` file.
1. Change `wal_level` value to `wal_level = logical`.
1. If `$GDK_ROOT/postgresql/data/replication.conf` exists, also change `wal_level = logical` there. (This file is only likely to exist if PostgreSQL had replication enabled at any point)
1. Restart the PostgreSQL service:

   ```shell
   gdk restart postgresql
   ```

## Start and configure Siphon

To start and configure Siphon:

1. Update `gdk.yml` to enable Siphon:

   ```shell
   gdk config set siphon.enabled true
   ```

1. Run `gdk reconfigure` to generate configuration files.
1. Optional. In GitLab Rails directory (`gitlab`), run the Siphon migration generator to generate a table. The migration generator creates a migration to create a table
   in ClickHouse.

   ```shell
   rails generate gitlab:click_house:siphon users
   ```

1. Run the ClickHouse migrations:

   ```shell
   bundle exec rake gitlab:clickhouse:migrate
   ```

1. Run `gdk start` to start the new service.

## Validate

To validate that Siphon is working in your GDK:

1. Check that GDK is running the `siphon-producer-main-db` and `siphon-clickhouse-consumer` services.
1. Inspect the output of these services with `gdk tail siphon-producer-main-db` and `gdk tail siphon-clickhouse-consumer`.
1. Add new data to the provisioned tables (`users` in this case).
1. Note that the data should be replicated to the `siphon_users` table in ClickHouse. Validate this using:

   ```sql
   SELECT COUNT(*) FROM siphon_users FINAL;
   ```
