# Zoekt

GitLab Enterprise Edition has a [Zoekt](https://github.com/sourcegraph/zoekt)
integration, which you can enable in your development environment.

## Installation

### Enable Zoekt in the GDK

The default version of Zoekt is automatically downloaded into your GDK root under `/zoekt`.
The default version of GitLab Zoekt Indexer is automatically downloaded into your GDK root under `/gitlab-zoekt-indexer`.

To enable the service and run it as part of `gdk start`:

1. Run `gdk config set zoekt.enabled true`.
1. Run `gdk reconfigure`.
1. Run `gdk start` which now starts 6 Zoekt servers:
   - `gitlab-zoekt-indexer` for test.
   - `gitlab-zoekt-indexer-1` for development.
   - `gitlab-zoekt-indexer-2` for development.
   - `zoekt-webserver` for test.
   - `zoekt-webserver-1` for development.
   - `zoekt-webserver-2` for development.

### Configure Zoekt in development

Zoekt must be enabled for each namespace you wish to index. Launch the Rails
console with `gdk rails c`. Given the default ports for Zoekt in GDK and
assuming your local instance has a namespace called `flightjs` (which is a GDK
seed by default), run the following from the Rails console:

```ruby
ApplicationSetting.current.update!(zoekt_indexing_enabled: true, zoekt_search_enabled: true, zoekt_auto_index_root_namespace: true, zoekt_indexing_paused: false)
zoekt_node = ::Search::Zoekt::Node.online.last
namespace = Namespace.find_by_full_path("flightjs") # Some namespace you want to enable
enabled_namespace = Search::Zoekt::EnabledNamespace.find_or_create_by(namespace: namespace)
zoekt_node.indices.find_or_create_by!(zoekt_enabled_namespace_id: enabled_namespace.id, namespace_id: namespace.id, zoekt_replica_id: Search::Zoekt::Replica.for_enabled_namespace!(enabled_namespace).id)
```

You can monitor the indexing progress via `bin/rails gitlab:zoekt:info`. When you see that replicas and indices are ready, you can perform the searches.

Now, if you create a new public project in the `flightjs` namespace or update
any existing public project in this namespace, it is indexed in Zoekt. Code
searches within this project are served by Zoekt.

Group-level searches in `flightjs` are also served by Zoekt.

### Switch to a different version of Zoekt

The default Zoekt version is defined in [`lib/gdk/config.rb`](../../lib/gdk/config.rb).

You can change this by setting `repo` and/or `version`:

```shell
   gdk config set zoekt.repo https://github.com/MyFork/zoekt.git
   gdk config set zoekt.version v1.2.3
```

Here, `repo` is any valid repository URL that can be cloned, and
`version` is any valid ref that can be checked out.

### Switch to a different version of GitLab Zoekt Indexer

The default GitLab Zoekt Indexer version is defined in [`lib/gdk/config.rb`](../../lib/gdk/config.rb).

To change this, set `indexer_version`:

```shell
   gdk config set zoekt.indexer_version v1.2.3
```

`indexer_version` is any valid ref that can be checked out.

### Test changes to Zoekt setup instructions without changing the development environment

To configure Zoekt in an environment without changing any of the settings
for your current environment, use [GDK-in-a-box](https://docs.gitlab.com/development/contributing/first_contribution/configure-dev-env-gdk-in-a-box/).

Before testing this, you must [configure a developer license in your GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/doc?ref_type=heads#configure-developer-license-in-gdk).

## Troubleshooting

### No preset version installed for command go

If you get this error during installation, execute the provided command
to install the correct version of Go:

```plaintext
No preset version installed for command go
Please install a version by running one of the following:
```

We cannot use the same Go version we use for other tools because the supported
version is controlled by Zoekt.
