# Using HashiCorp Vault with GDK

As of October 2024, HashiCorp Vault is no longer supported in GDK. Use
[OpenBao](./openbao.md) instead.
