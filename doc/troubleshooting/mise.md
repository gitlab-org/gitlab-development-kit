# Troubleshooting mise

The following are possible solutions to problems you might encounter with
[mise](https://mise.jdx.dev/) and GDK.

If your issue is not listed here:

- For generic mise problems, raise an issue or pull request in the [mise project](https://github.com/jdx/mise).
- For GDK-specific issues, raise an issue or merge request in the [GDK project](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues).

If you are a GitLab team member, you can also ask for help with troubleshooting in
the `#mise` Slack channel. If your problem is GDK-specific, use the
`#gdk` channel so more people can see it.

## Error: `No such file or directory` when installing

You might have `mise install` fail with a cache error like the following.

```shell
$ mise install
mise ruby build tool update error: failed to update ruby-build: No such file or directory (os error 2)
mise failed to execute command: ~/Library/Caches/mise/ruby/ruby-build/bin/ruby-build 3.2.5 /Users/gdk/.local/share/mise/installs/ruby/3.2.5
mise No such file or directory (os error 2)
```

You can usually fix this by cleaning the mise cache: `mise cache clear`

## Error `~/.local/share/mise/plugins/yarn/bin/list-all: timed out: timed out waiting on channel`

If you use SSH to connect to GitHub, `yarn` might fail because of a timeout. You can temporarily use HTTPS by commenting out the following lines in `~/.gitconfig`:

```shell
#[url "git@github.com:"]
#    insteadof = https://github.com/
```

## Error: `command not found: gdk` or `mise is not activated`

Check steps in <https://mise.jdx.dev/getting-started.html#activate-mise> to ensure you have activated `mise` correctly.

If you use `zsh` than this should do the trick:

```shell
echo 'eval "$(mise activate zsh)"' >> ~/.zshrc
```
