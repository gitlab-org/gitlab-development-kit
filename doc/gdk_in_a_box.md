# GDK-in-a-box

GDK-in-a-box provides a preconfigured virtual machine you can download and boot
to instantly start developing.

For information on downloading, installing, and connecting to GDK-in-a-box, see the
[GitLab developer documentation](https://docs.gitlab.com/ee/development/contributing/first_contribution/configure-dev-env-gdk-in-a-box.html).

## Troubleshoot

If you have any issues, the simplest and fastest solution is to:

- Delete the virtual machine.
- Download the latest build.
- Follow the [standard setup instructions](https://docs.gitlab.com/ee/development/contributing/first_contribution/configure-dev-env-gdk-in-a-box.html).

Some troubleshooting solutions are detailed in the [feedback issue](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/2035).

NOTE:

Your GitLab instance will be restored to defaults.
Be sure to commit and push all changes beforehand, or they will be lost.

## Building GDK-in-a-box

This isn't necessary to use GDK-in-box.
Follow the [instructions to build a new version of GDK-in-a-box](build_gdk_in_a_box.md).
