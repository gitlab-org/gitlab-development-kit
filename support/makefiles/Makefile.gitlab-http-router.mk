gitlab_http_router_dir = ${gitlab_development_root}/gitlab-http-router

.PHONY: gitlab-http-router-setup
ifeq ($(gitlab_http_router_enabled),true)
gitlab-http-router-setup: gitlab-http-router-setup-timed
else
gitlab-http-router-setup:
	@true
endif

.PHONY: gitlab-http-router-setup-run
gitlab-http-router-setup-run: gitlab-http-router/.git gitlab-http-router-common-setup

gitlab-http-router/.git:
	$(Q)support/component-git-clone ${git_params} ${gitlab_http_router_repo} gitlab-http-router

.PHONY: gitlab-http-router-common-setup
gitlab-http-router-common-setup: touch-examples gitlab-http-router-npm-install

.PHONY: gitlab-http-router-npm-install
gitlab-http-router-npm-install: gitlab-http-router-asdf-install
	@echo
	@echo "${DIVIDER}"
	@echo "Performing npm steps for ${gitlab_http_router_dir}"
	@echo "${DIVIDER}"
	$(Q)cd ${gitlab_http_router_dir} && npm install

.PHONY: gitlab-http-router-asdf-install
gitlab-http-router-asdf-install:
ifeq ($(asdf_opt_out),false)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing asdf tools from ${gitlab_http_router_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${gitlab_http_router_dir} && ASDF_DEFAULT_TOOL_VERSIONS_FILENAME="${gitlab_http_router_dir}/.tool-versions" $(ASDF_INSTALL)
	$(Q)cd ${gitlab_http_router_dir} && asdf reshim
else ifeq ($(mise_enabled),true)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing mise tools from ${gitlab_http_router_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${gitlab_http_router_dir} && $(MISE_INSTALL)
else
	@echo "No tooling manager found for ${gitlab_http_router_dir}"
	@true
endif

.PHONY: gitlab-http-router-update
ifeq ($(gitlab_http_router_enabled),true)
gitlab-http-router-update: gitlab-http-router-update-timed
else
gitlab-http-router-update:
	@true
endif

.PHONY: ensure-gitlab-http-router-stopped
ensure-gitlab-http-router-stopped:
	@echo
	@echo "${DIVIDER}"
	@echo "Ensuring gitlab-http-router is stopped"
	@echo "See https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/2159"
	@echo "${DIVIDER}"
	$(Q)gdk stop gitlab-http-router

.PHONY: gitlab-http-router-update-run
gitlab-http-router-update-run: ensure-gitlab-http-router-stopped gitlab-http-router/.git/pull gitlab-http-router-common-setup
	$(Q)gdk restart gitlab-http-router

.PHONY: gitlab-http-router/.git/pull
gitlab-http-router/.git/pull: gitlab-http-router/.git
	@echo
	@echo "${DIVIDER}"
	@echo "Updating ${gitlab_http_router_dir}"
	@echo "${DIVIDER}"
	$(Q)support/component-git-update gitlab_http_router gitlab-http-router main main
