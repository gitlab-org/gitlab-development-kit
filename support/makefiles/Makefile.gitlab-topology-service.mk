gitlab_topology_service_dir = ${gitlab_development_root}/gitlab-topology-service

.PHONY: gitlab-topology-service-setup
ifeq ($(gitlab_topology_service_enabled),true)
gitlab-topology-service-setup: gitlab-topology-service-setup-timed gitlab-topology-service/config.toml
else
gitlab-topology-service-setup:
	@true
endif

.PHONY: gitlab-topology-service-setup-run
gitlab-topology-service-setup-run: gitlab-topology-service/.git gitlab-topology-service-common-setup

gitlab-topology-service/.git:
	$(Q)rm -fr gitlab-topology-service/config.toml
	$(Q)support/component-git-clone ${git_params} ${gitlab_topology_service_repo} gitlab-topology-service

.PHONY: gitlab-topology-service-common-setup
gitlab-topology-service-common-setup: touch-examples gitlab-topology-service/config.toml gitlab-topology-service-make-deps-install

.PHONY: gitlab-topology-service-make-deps-install
gitlab-topology-service-make-deps-install: gitlab-topology-service-asdf-install
	@echo
	@echo "${DIVIDER}"
	@echo "Performing make deps steps for ${gitlab_topology_service_dir}"
	@echo "${DIVIDER}"
	$(Q)support/asdf-exec ${gitlab_topology_service_dir} make deps

.PHONY: gitlab-topology-service-asdf-install
gitlab-topology-service-asdf-install:
ifeq ($(asdf_opt_out),false)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing asdf tools from ${gitlab_topology_service_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q). support/bootstrap-common.sh; cd ${gitlab_topology_service_dir}; asdf_install_update_plugins
	$(Q)cd ${gitlab_topology_service_dir} && ASDF_DEFAULT_TOOL_VERSIONS_FILENAME="${gitlab_topology_service_dir}/.tool-versions" $(ASDF_INSTALL)
else ifeq ($(mise_enabled),true)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing mise tools from ${gitlab_topology_service_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${gitlab_topology_service_dir} && $(MISE_INSTALL)
else
	@true
endif

.PHONY: gitlab-topology-service-update
ifeq ($(gitlab_topology_service_enabled),true)
gitlab-topology-service-update: gitlab-topology-service-update-timed
else
gitlab-topology-service-update:
	@true
endif

.PHONY: gitlab-topology-service-update-run
gitlab-topology-service-update-run: gitlab-topology-service/.git/pull gitlab-topology-service-common-setup gitlab-topology-service/config.toml

.PHONY: gitlab-topology-service/.git/pull
gitlab-topology-service/.git/pull: gitlab-topology-service/.git
	@echo
	@echo "${DIVIDER}"
	@echo "Updating ${gitlab_topology_service_dir}"
	@echo "${DIVIDER}"
	$(Q)support/component-git-update gitlab_topology_service gitlab-topology-service main main
