workhorse_dir = ${gitlab_development_root}/gitlab/workhorse

.PHONY: gitlab-workhorse-setup
gitlab-workhorse-setup:
ifeq ($(workhorse_skip_setup),true)
	@echo
	@echo "${DIVIDER}"
	@echo "Skipping gitlab-workhorse setup due to workhorse.skip_setup set to true"
	@echo "${DIVIDER}"
else
	$(Q)$(MAKE) gitlab-workhorse-asdf-install gitlab/workhorse/gitlab-workhorse gitlab/workhorse/config.toml
endif

.PHONY: gitlab-workhorse-update
gitlab-workhorse-update: gitlab-workhorse-update-timed

.PHONY: gitlab-workhorse-update-run
gitlab-workhorse-update-run: gitlab-workhorse-setup

.PHONY: gitlab-workhorse-asdf-install
gitlab-workhorse-asdf-install:
ifeq ($(asdf_opt_out),false)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing asdf tools from ${workhorse_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${workhorse_dir} && ASDF_DEFAULT_TOOL_VERSIONS_FILENAME="${workhorse_dir}/.tool-versions" $(ASDF_INSTALL)
else ifeq ($(mise_enabled),true)
	@echo
	@echo "${DIVIDER}"
	@echo "Installing tools from ${workhorse_dir}/.tool-versions"
	@echo "${DIVIDER}"
	$(Q)cd ${workhorse_dir} && $(MISE_INSTALL)
else
	@true
endif

.PHONY: gitlab-workhorse-clean-bin
gitlab-workhorse-clean-bin:
	$(Q)support/asdf-exec gitlab/workhorse $(MAKE) clean

.PHONY: gitlab/workhorse/gitlab-workhorse
gitlab/workhorse/gitlab-workhorse:
ifeq ($(workhorse_skip_compile),true)
	@echo
	@echo "${DIVIDER}"
	@echo "Downloading gitlab-workhorse binaries (workhorse.skip_compile set to true)"
	@echo "${DIVIDER}"
	$(Q)support/package-helper workhorse download
else
	$(Q)$(MAKE) gitlab-workhorse-clean-bin
	@echo
	@echo "${DIVIDER}"
	@echo "Compiling gitlab/workhorse/gitlab-workhorse"
	@echo "${DIVIDER}"
	$(Q)support/asdf-exec gitlab/workhorse $(MAKE) ${QQ}
endif
