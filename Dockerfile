FROM ubuntu:22.04
LABEL authors.maintainer="GDK contributors: https://gitlab.com/gitlab-org/gitlab-development-kit/-/graphs/main"

## The CI script that build this file can be found under: support/docker

ARG TOOL_VERSION_MANAGER

ENV DEBIAN_FRONTEND=noninteractive
ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US.UTF-8
ENV TOOL_VERSION_MANAGER=${TOOL_VERSION_MANAGER}

RUN apt-get update && \
    apt-get install -y \
      curl \
      libssl-dev \
      locales \
      locales-all \
      pkg-config \
      software-properties-common \
      sudo && \
    add-apt-repository ppa:git-core/ppa -y

RUN useradd --user-group --create-home --groups sudo gdk && \
    echo "gdk ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gdk_no_password

WORKDIR /home/gdk/tmp
RUN chown -R gdk:gdk /home/gdk

USER gdk
COPY --chown=gdk . .

ENV PATH="/home/gdk/.local/bin:/home/gdk/.local/share/mise/bin:/home/gdk/.local/share/mise/shims:/home/gdk/.asdf/shims:/home/gdk/.asdf/bin:${PATH}"

# Perform bootstrap
# Verify that tools are available
# Remove unneeded packages
# Remove caches & copied files
# Remove all files from "$HOME/gdk/gitaly/_build" except the compiled binaries in "bin"
RUN bash ./support/bootstrap && \
    bash -eclx "${TOOL_VERSION_MANAGER} version; yarn --version; node --version; ruby --version" && \
    sudo apt-get purge software-properties-common -y && \
    sudo apt-get clean -y && \
    sudo apt-get autoremove -y && \
    sudo rm -rf \
      "$HOME/.asdf/tmp/"* \
      "$HOME/.cache/" \
      "$HOME/tmp" \
      /tmp/* \
      /var/cache/apt/* \
      /var/lib/apt/lists/* \
      $(ls -d "$HOME/gdk/gitaly/_build/"* | grep -v /bin)

WORKDIR /home/gdk
