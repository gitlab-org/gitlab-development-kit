# frozen_string_literal: true

RSpec.describe GDK::Services::Sshd do
  describe '#name' do
    it { expect(subject.name).to eq('sshd') }
  end

  describe '#command' do
    before do
      stub_gdk_yaml <<~YAML
        sshd:
          use_gitlab_sshd: #{use_gitlab_sshd}
      YAML
    end

    context 'when gitlab-sshd is disabled' do
      let(:use_gitlab_sshd) { false }

      it { expect(subject.command).to eq("#{GDK.config.sshd.bin} -e -D -f #{GDK.config.gdk_root.join('openssh', 'sshd_config')}") }
    end

    context 'when gitlab-sshd is enabled' do
      let(:use_gitlab_sshd) { true }

      it { expect(subject.command).to eq("#{GDK.config.gitlab_shell.dir}/bin/gitlab-sshd -config-dir #{GDK.config.gitlab_shell.dir}") }
    end
  end
end
