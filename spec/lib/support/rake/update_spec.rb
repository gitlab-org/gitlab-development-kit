# frozen_string_literal: true

RSpec.describe Support::Rake::Update do
  before do
    stub_gdk_yaml({})
  end

  describe '.make_tasks' do
    it 'returns all make targets' do
      expect(described_class.make_tasks.map(&:target)).to match_array(%w[
        gitlab-git-pull
        gitlab-setup
        postgresql
        gitlab/doc/api/graphql/reference/gitlab_schema.json
        preflight-checks
        preflight-update-checks
        gitaly-update
        ensure-databases-setup
        gitlab-shell-update
        unlock-dependency-installers
        gitlab-http-router-update
        gitlab-topology-service-update
        docs-gitlab-com-update
        gitlab-elasticsearch-indexer-update
        gitlab-k8s-agent-update
        gitlab-pages-update
        gitlab-translations-unlock
        gitlab-ui-update
        gitlab-workhorse-update
        gitlab-zoekt-indexer-update
        gitlab-ai-gateway-update
        grafana-update
        jaeger-update
        object-storage-update
        pgvector-update
        zoekt-update
        duo-workflow-service-update
        openbao-update
        siphon-update
        nats-update
        gitlab-runner-update
      ])
    end

    it 'notes which tasks a skipped by default' do
      expect(described_class.make_tasks.filter(&:skip?).map(&:target)).to match_array(%w[
        docs-gitlab-com-update
        gitlab-elasticsearch-indexer-update
        gitlab-k8s-agent-update
        gitlab-pages-update
        gitlab-ui-update
        gitlab-zoekt-indexer-update
        gitlab-ai-gateway-update
        grafana-update
        jaeger-update
        object-storage-update
        pgvector-update
        zoekt-update
        duo-workflow-service-update
        openbao-update
        siphon-update
        nats-update
        gitlab-runner-update
      ])
    end

    context 'when a corresponding default-disabled service is enabled' do
      before do
        stub_gdk_yaml({
          'openbao' => { 'enabled' => 'true' }
        })
      end

      it 'no longer notes that task as skipped' do
        expect(described_class.make_tasks.filter(&:skip?).map(&:target)).not_to include('openbao-update')
      end
    end
  end
end
