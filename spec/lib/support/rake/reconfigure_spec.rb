# frozen_string_literal: true

RSpec.describe Support::Rake::Reconfigure do
  before do
    stub_gdk_yaml({})
  end

  describe '.make_tasks' do
    it 'returns all make targets' do
      expect(described_class.make_tasks.map(&:target)).to match_array(%w[
        Procfile
        jaeger-setup
        postgresql
        openssh-setup
        nginx-setup
        registry-setup
        elasticsearch-setup
        gitlab-runner-setup
        runner-setup
        geo-config
        gitlab-topology-service-setup
        gitlab-http-router-setup
        docs-gitlab-com-setup
        gitlab-observability-backend-setup
        gitlab-elasticsearch-indexer-setup
        gitlab-k8s-agent-setup
        gitlab-pages-setup
        gitlab-ui-setup
        gitlab-zoekt-indexer-setup
        grafana-setup
        object-storage-setup
        openldap-setup
        pgvector-setup
        prom-setup
        snowplow-micro-setup
        zoekt-setup
        duo-workflow-service-setup
        duo-workflow-executor-setup
        postgresql-replica-setup
        postgresql-replica-2-setup
        openbao-setup
        gdk-reconfigure-task
        siphon-setup
        nats-setup
      ])
    end
  end
end
