# frozen_string_literal: true

RSpec.describe GDK::Diagnostic::ToolVersionManager do
  let(:asdf_opt_out) { false }
  let(:mise_enabled) { false }
  let(:asdf_version) { 'version: v0.15.0' }

  before do
    allow(subject).to receive(:current_asdf_version).and_return(asdf_version)
    allow(GDK.config).to receive_message_chain(:asdf, :opt_out?).and_return(asdf_opt_out)
    allow(GDK.config).to receive_message_chain(:mise, :enabled?).and_return(mise_enabled)
  end

  describe '#success?' do
    context 'when mise is enabled and asdf is opted out' do
      let(:asdf_opt_out) { true }
      let(:mise_enabled) { true }

      it 'returns true' do
        expect(subject).to be_success
      end
    end

    context 'when a custom tool version manager is used' do
      let(:asdf_opt_out) { true }
      let(:mise_enabled) { false }

      it 'returns true' do
        expect(subject).to be_success
      end
    end

    context 'when asdf is enabled and mise is disabled' do
      let(:asdf_opt_out) { false }
      let(:mise_enabled) { false }

      it 'returns false' do
        expect(subject).not_to be_success
      end
    end
  end

  describe '#detail' do
    context 'when mise is enabled and asdf is opted out' do
      let(:asdf_opt_out) { true }
      let(:mise_enabled) { true }

      it 'returns no message' do
        expect(subject.detail).to be_nil
      end
    end

    context 'when a custom tool version manager is used' do
      let(:asdf_opt_out) { true }
      let(:mise_enabled) { false }

      it 'returns no message' do
        expect(subject.detail).to be_nil
      end
    end

    context 'when asdf is enabled and mise is disabled' do
      let(:asdf_opt_out) { false }
      let(:mise_enabled) { false }

      it 'returns a message' do
        expected = <<~MESSAGE
          Starting April 1st, 2025, mise will replace asdf as the supported tool version manager for GDK.

          The main benefit of mise is better performance, and it removes the manual setup we had to do
          with asdf, like installing dependencies in the right order when they depend on each other.
          There's no need to install plugins separately as mise takes care of everything.

          You are currently using asdf. To migrate to mise, run:

          1. Go to your GDK directory:
            cd "#{GDK.config.gdk_root}"

          2. Run the migration task:
            bundle exec rake mise:migrate

          If you run into any issues during migration, please let us know in this issue: https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/2360.
        MESSAGE

        expect(subject.detail).to eq(expected)
      end

      context 'with broken asdf version' do
        let(:asdf_version) { 'version: v0.16.0' }

        it 'returns an additional error' do
          expect(subject.detail).to include(<<~ERROR)
            ERROR: Your installed version of asdf (`#{asdf_version}`) has a bug that makes it incompatible with GDK.
            Please downgrade to `v0.15.0` or switch to `mise`.

            Starting April 1st, 2025, mise will replace asdf as the supported tool version manager for GDK.
          ERROR
        end
      end
    end
  end
end
