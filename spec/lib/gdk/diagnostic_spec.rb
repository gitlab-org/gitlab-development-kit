# frozen_string_literal: true

RSpec.describe GDK::Diagnostic do
  describe '.all' do
    it 'creates instances of all GDK::Diagnostic classes' do
      expect { described_class.all }.not_to raise_error
    end

    it 'contains only diagnostic classes' do
      diagnostic_classes = (GDK::Diagnostic.constants - [:Base]).map do |const|
        GDK::Diagnostic.const_get(const)
      end

      expect(described_class.all.map(&:class)).to eq(diagnostic_classes)
    end
  end
end
