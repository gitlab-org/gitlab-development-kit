# frozen_string_literal: true

RSpec.describe 'rake gdk:migrate' do
  before(:all) do
    Rake.application.rake_require('tasks/gdk')
  end

  it 'invokes its dependencies' do
    expect(task.prerequisites).to eq(%w[
      migrate:fix_telemetry_user
    ])
  end
end

RSpec.describe 'rake gdk:migrate:fix_telemetry_user' do
  let(:username) { 'some user' }

  before(:all) do
    Rake.application.rake_require('tasks/gdk')
  end

  before do
    stub_gdk_yaml(<<~YAML)
      telemetry:
        enabled: #{enabled}
        username: #{username}
    YAML

    allow(GDK.config).to receive(:save_yaml!)
  end

  context 'when enabled' do
    let(:enabled) { true }

    context 'when hardcoded' do
      let(:username) { 'telemetry_user' }

      it 'shuffles a new username' do
        expect { task.invoke }.to output(/Fixing hardcoded telemetry username./).to_stdout

        expect(GDK.config.telemetry.username).not_to eq(username)
        expect(GDK.config.telemetry.username).not_to match(/^\h{16}$/)
      end
    end

    context 'when not hardcoded' do
      it 'does not fix it' do
        task.invoke

        expect(GDK.config.telemetry.username).to eq(username)
      end
    end
  end

  context 'when not enabled' do
    let(:enabled) { false }

    it 'does not fix it' do
      task.invoke

      expect(GDK.config.telemetry.username).to eq(username)
    end
  end
end
